var my_screensharing_session;

// The following DOM node will be observed to detect changes.
// var my_target_node = document.querySelector('body');
var my_target_node = $('body')[0];

var my_robot_ip_address = 'http://localhost/';

/* ****************************************************************************************************************** */

// Starts the screensharing
var myStartRobotScreenSharing = function () {
 
  // alert('myStartRobotScreenSharing() starts');
 
  // Store the current horizontal and vertical scrolling position in data attributes of the body tag.
  $('body').attr('data-scrollx', window.scrollX);
	 $('body').attr('data-scrolly', window.scrollY);

  // Update the data attributes with the new scrolling position when the user scrolls the document in 
  // the browser window.
  $(window).on('scroll', function(e) {
   
    $('body').attr('data-scrollx', window.scrollX);
    $('body').attr('data-scrolly', window.scrollY);

  });

  // Event handler for playing videos.
  $('video').on('play', function(e) {
   
    $(this).play();
    
  });

	 myStartObservation();

	 // Set value when typing in input fields
  $('input').on('input', function(e) {
   
    this.setAttribute('value', e.target.value);

  });

	 myMutationObserverCallback();
  
}; // myStartRobotScreenSharing()

/* ****************************************************************************************************************** */

// Begin observation of DOM for changes.
var myStartObservation = function () {

  // Create new MutationObserver instance. 
	 var my_observer = new MutationObserver(function(my_changes) {
   
    // Run callback function.
			 myMutationObserverCallback(my_changes);

		});

  // Specify aspects to be included in the observation.
	 var my_observation_configuration = { childList: true, subtree: true, attributes: true, characterData: true };

  // Begin observation.
	 my_observer.observe(my_target_node, my_observation_configuration);

}; // myStartObservation()

/* ****************************************************************************************************************** */

// Called everytime a change in the DOM occurs.
var myMutationObserverCallback = function (my_changes) {
 
  // Send the HTML contents of the current page.       
  mySendSnapshot();
 
} // myMutationObserverCallback()

/* ****************************************************************************************************************** */

// Send the HTML contents of the current page.       
var mySendSnapshot = function () {
 
  // Generate inline CSS styles from styles of the tabletview.
		var my_css_string = myGenerateCSSString();

  var my_absolute_url_pattern = /^https?:\/\/|^\/\//i;

  var my_snapshot_selection = $('body').clone();

  // Remove script, button, and stylesheet link tags.  
  $('script', my_snapshot_selection).remove();
  $('button', my_snapshot_selection).remove();
  $('link[rel="stylesheet"]', my_snapshot_selection).remove();
  
  // Prepend generated CSS string.
  my_snapshot_selection.prepend('<style>' + my_css_string + '</style>');

  // Prepend URL prefix beginning with the robot's IP address to any image source so that the image can be retrieved from the robot.
  my_snapshot_selection.find('img').each(function() {
  
    // If the image source is not an absolute URL, then prepend URL prefix of the image's location on the robot.
    if (!my_absolute_url_pattern.test($(this).attr('src'))) {
     
       $(this).attr('src', my_robot_ip_address + $(this).attr('src'));
       
    } // if
    
  });

  // Store HTML of snapshot for sending.
  var my_final_html = my_snapshot_selection.html();

  // Get ALMemory service.
  my_screensharing_session.service('ALMemory').then(function(my_memory) {
   
    // Raise event to send the current snapshot to ALMemory.
    my_memory.raiseEvent('ScreenShare/CurrentSnapshot', my_final_html);
    
  });

} // mySendSnapshot()

/* ****************************************************************************************************************** */

// Generate a CSS string from the stylesheets in the document.
var myGenerateCSSString = function () {

  var my_return_css_string = '';
  
 	Array.prototype.forEach.call(document.styleSheets, function(my_stylesheet) {
   
    // Stylesheet URL contains localhost. ==> Nothing to do.
    if (my_stylesheet.href.indexOf('localhost') !== -1) {
     
       return;
     
    } // if
    
    // No href attribute or no css rules => Nothing to do.
    if (!my_stylesheet.href || !my_stylesheet.cssRules) {
     
       return;
    
    } // if
    
    Array.prototype.forEach.call(my_stylesheet.cssRules, function (my_css_rule) {
     
      // The CSS rule contains "import @url".
      if (my_css_rule.cssText.indexOf('import') !== -1) {
       
         // Append the text of the rule to the result string.
         my_return_css_string += my_css_rule.cssText;
         return;
         
      } // if

      my_new_css_string = my_css_rule.cssText.replace(/url\(\s*[\'"]?\/?(.+?)[\'"]?\s*\)/g, function(my_match) { return replace_rel_css_url_abs(my_match, my_stylesheet); });
      
      my_return_css_string += my_new_css_string;

    });
 
  });
  
  return my_return_css_string;
  
}; // myGenerateCSSString()

/* ****************************************************************************************************************** */

// The main function. 
(function() {
  
  try {
 
    QiSession(function(s) {

      my_screensharing_session = s;
      
      // Get ALMemory service.
      s.service('ALMemory').then(function(my_memory) {
       
        my_memory.subscriber('ScreenShare/SendIPAddress').then(function(my_subscriber) {
         
          my_subscriber.signal.connect(function(my_sent_ip_address) {
           
            my_robot_ip_address = my_sent_ip_address;
            myStartRobotScreenSharing();
            
          });
         
        });
        
      });
      
    });
    
  } // try

  // Error handling.
  catch (my_error) {
   
    console.log('Error when initializing QiSession: ' + my_error.message);

  } // catch
   

  // Request the robot's IP address after 1,000 milliseconds (= 1 second).
  setTimeout(function() {

    // Get ALMemory service.
    my_screensharing_session.service('ALMemory').then(function(my_memory) {
     
      // Raise event to retrieve the robot's IP address.
      my_memory.raiseEvent('ScreenShare/GetIPAddress', 1);
      
    });
    
  }, 1000);
		
})();

/* ****************************************************************************************************************** */
